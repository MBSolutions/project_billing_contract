# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields
from trytond.pyson import Eval, In


class Contract(ModelWorkflow, ModelSQL, ModelView):
    _name = 'contract.contract'

    projects = fields.One2Many('project.work', 'contract', 'Projects',
            context={
                'type': 'project',
                'party': Eval('party_invoice')
                }, depends=['party_invoice', 'party_service', 'state'],
            domain=[
                ('party', 'in', [Eval('party_invoice'), Eval('party_service')])
                ],
            states={
                'readonly': In(Eval('state'), ['cancel', 'terminated'])
                })

    def __init__(self):
        super(Contract, self).__init__()
        self._error_messages.update({
            # TODO: Add project.code to error_args when code has been added to
            # the project base module.
            'open_projects': 'A contract can only be terminated if all related '
                    'projects are closed! Affected project: %s',
            })

    def check_terminate(self, contract_id):
        # all projects must be closed to *terminate* a contract
        res = super(Contract, self).check_terminate(contract_id)
        if res:
            contract = self.browse(contract_id)
            if contract.projects:
                for project in contract.projects:
                    if project.state != 'done':
                        # TODO: Add project.code to error_args when code
                        # has been added to the project base module.
                        self.raise_user_error('open_projects',
                            error_args=(project.name,))
        return res

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['projects'] = False
        return super(Contract, self).copy(ids, default=default)

Contract()
